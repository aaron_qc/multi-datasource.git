package com.hb.ocean.controller;

import com.hb.ocean.mapper.account.UserMapper;
import com.hb.ocean.mapper.iceberg.ItemOrderMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author QuCheng on 2019-09-17.
 */
@RestController
@RequestMapping("/qc")
public class TestView {

    /**
     * 不推荐直接在controller层引入Dao层。这里只做演示使用
     */
    @Resource
    private UserMapper userMapper;
    @Resource
    private ItemOrderMapper itemOrderMapper;

    @GetMapping("/test")
    public String test() {
        System.out.println("用户数：" + userMapper.selectCountUser(null, null));
        System.out.println("订单数：" + itemOrderMapper.selectCountSuccessOrder(null, null));
        return "ok";
    }
}
